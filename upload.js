accessid = '';
accesskey = '';
host = '';
policyBase64 = '';
signature = '';
callbackbody = '';
filename = '';
key = '';
expire = 0;
g_object_name = '';
g_object_name_type = '';
now = timestamp = Date.parse(new Date()) / 1000;

function send_request() {
  var xmlhttp = null;
  if (window.XMLHttpRequest) {
    xmlhttp = new XMLHttpRequest();
  } else if (window.ActiveXObject) {
    xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
  }

  if (xmlhttp != null) {
    // serverUrl是 用户获取 '签名和Policy' 等信息的应用服务器的URL，请将下面的IP和Port配置为您自己的真实信息。
    serverUrl = 'http://139.159.159.247:8080/user/oss/policy';

    xmlhttp.open('GET', serverUrl, false);
    xmlhttp.send(null);
    return xmlhttp.responseText;
  } else {
    alert('Your browser does not support XMLHTTP.');
  }
}

function check_object_radio() {
  var tt = document.getElementsByName('myradio');
  for (var i = 0; i < tt.length; i++) {
    if (tt[i].checked) {
      g_object_name_type = tt[i].value;
      break;
    }
  }
}

function get_signature() {
  // 可以判断当前expire是否超过了当前时间， 如果超过了当前时间， 就重新取一下，3s 作为缓冲。
  now = timestamp = Date.parse(new Date()) / 1000;
  if (expire < now + 3) {
    body = send_request();

    let obj = JSON.parse(body).data;
    // var obj = eval('(' + body + ')');
    host = obj['host'];
    policyBase64 = obj['policy'];
    accessid = obj['accessid'];
    signature = obj['signature'];
    expire = parseInt(obj['expire']);
    callbackbody = obj['callback'];
    key = obj['dir'];
    return true;
  }
  return false;
}

function random_string(len) {
  len = len || 32;
  var chars = 'ABCDEFGHJKMNPQRSTWXYZabcdefhijkmnprstwxyz2345678';
  var maxPos = chars.length;
  var pwd = '';
  for (i = 0; i < len; i++) {
    pwd += chars.charAt(Math.floor(Math.random() * maxPos));
  }
  return pwd;
}

function get_suffix(filename) {
  pos = filename.lastIndexOf('.');
  suffix = '';
  if (pos != -1) {
    suffix = filename.substring(pos);
  }
  return suffix;
}

function calculate_object_name(filename) {
  suffix = get_suffix(filename);
  g_object_name = key + random_string(10) + suffix;
  return '';
}

function get_uploaded_object_name(filename) {
  return g_object_name;
}

function set_upload_param(up, filename, ret) {
  if (ret == false) {
    ret = get_signature();
  }
  g_object_name = key;
  if (filename != '') {
    suffix = get_suffix(filename);
    calculate_object_name(filename);
  }
  new_multipart_params = {
    key: g_object_name,
    policy: policyBase64,
    OSSAccessKeyId: accessid,
    success_action_status: '200', //让服务端返回200,不然，默认会返回204
    callback: callbackbody,
    signature: signature,
  };

  up.setOption({
    url: host,
    multipart_params: new_multipart_params,
  });

  let msg = up.start();
}

// 点击上传文件清空错误提示
let btn = (document.getElementById('selectfiles').onclick = function () {
  deleteChild();
});

function deleteChild() {
  let e = document.getElementById('console');
  e.innerHTML = '';
}
// 上传文件成功之后，发布下载链接
function publishLink() {
  const options = {
    clean: false, // true: 清除会话, false: 保留会话
    connectTimeout: 1000, // 超时时间
    // 认证信息
    clientId: `cxt_${Math.random().toString(16).slice(3)}`, //客户端ID
    username: 'admin', //连接用户名
    password: 'public', //连接密码，默认为public,新版本登录后台界面会让你修改密码
    // 心跳时间
    keepalive: 30,
  };

  // 连接字符串, 通过协议指定使用的连接方式
  const connectUrl = 'ws://139.159.159.247:8083/mqtt'; //连接服务端地址，注意查看ws协议对应的端口号
  const client = mqtt.connect(connectUrl, options);

  client.on('connect', () => {
    console.log('connect success');
    // 发布消息
    let obj = {
      msg: window.host + '/' + window.g_object_name,
    };
    let link = JSON.stringify(obj); // 后端规定这里必须要用JSON字符串
    let topic =
      g_object_name_type == 'little_tube'
        ? '/LITTLE_TUBE_TO_HWY'
        : '/BIG_TUBE_TO_HWY';
    client.publish(topic, link, (err) => {
      console.log(err || 'publish success');
    });
  });
  //失败重连
  client.on('reconnect', (error) => {
    console.log('reconnect again:', error);
  });
  //连接失败
  client.on('error', (error) => {
    console.log('connect failed:', error);
  });
}

var uploader = new plupload.Uploader({
  runtimes: 'html5,flash,silverlight,html4',
  browse_button: 'selectfiles',
  //multi_selection: false,
  container: document.getElementById('container'),
  flash_swf_url: 'lib/plupload-2.1.2/js/Moxie.swf',
  silverlight_xap_url: 'lib/plupload-2.1.2/js/Moxie.xap',
  url: 'http://oss.aliyuncs.com',

  filters: {
    mime_types: [
      //只允许上传图片和zip文件
      { title: 'Image files', extensions: 'jpg,gif,png,bmp' },
      { title: 'Zip files', extensions: 'zip,rar' },
      { title: 'bin files', extensions: 'bin' },
      { title: 'hex files', extensions: 'hex' },
    ],
    max_file_size: '10mb', //最大只能上传10mb的文件
    prevent_duplicates: true, //不允许选取重复文件
  },

  init: {
    PostInit: function () {
      document.getElementById('ossfile').innerHTML = '';
      document.getElementById('postfiles').onclick = function () {
        set_upload_param(uploader, '', false);
        return false;
      };
    },

    FilesAdded: function (up, files) {
      plupload.each(files, function (file) {
        document.getElementById('ossfile').innerHTML +=
          '<div id="' +
          file.id +
          '">' +
          file.name +
          ' (' +
          plupload.formatSize(file.size) +
          ')<b></b>' +
          '<div class="progress"><div class="progress-bar" style="width: 0%"></div></div>' +
          '</div>';
      });
    },

    BeforeUpload: function (up, file) {
      check_object_radio();
      set_upload_param(up, file.name, true);
    },

    UploadProgress: function (up, file) {
      var d = document.getElementById(file.id);
      d.getElementsByTagName('b')[0].innerHTML =
        '<span>' + file.percent + '%</span>';
      var prog = d.getElementsByTagName('div')[0];
      var progBar = prog.getElementsByTagName('div')[0];
      progBar.style.width = 2 * file.percent + 'px';
      progBar.setAttribute('aria-valuenow', file.percent);
    },

    FileUploaded: function (up, file, info) {
      if (info.status == 200) {
        document
          .getElementById(file.id)
          .getElementsByTagName('b')[0].innerHTML =
          'upload to oss success, object name:' +
          get_uploaded_object_name(file.name) +
          ' 回调服务器返回的内容是:' +
          info.response;
        publishLink();
      } else if (info.status == 203) {
        document
          .getElementById(file.id)
          .getElementsByTagName('b')[0].innerHTML =
          '上传到OSS成功，但是oss访问用户设置的上传回调服务器失败，失败原因是:' +
          info.response;
      } else {
        document
          .getElementById(file.id)
          .getElementsByTagName('b')[0].innerHTML = info.response;
      }
    },

    Error: function (up, err) {
      if (err.code == -600) {
        document
          .getElementById('console')
          .appendChild(
            document.createTextNode(
              '\n选择的文件太大了,可以根据应用情况，在upload.js 设置一下上传的最大大小'
            )
          );
      } else if (err.code == -601) {
        document
          .getElementById('console')
          .appendChild(
            document.createTextNode(
              '\n选择的文件后缀不对,可以根据应用情况，在upload.js进行设置可允许的上传文件类型'
            )
          );
      } else if (err.code == -602) {
        document
          .getElementById('console')
          .appendChild(document.createTextNode('\n这个文件已经上传过一遍了'));
      } else {
        document
          .getElementById('console')
          .appendChild(document.createTextNode('\nError xml:' + err.response));
      }
    },
  },
});

uploader.init();
